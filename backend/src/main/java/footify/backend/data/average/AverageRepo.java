package footify.backend.data.average;

import org.springframework.data.repository.CrudRepository;
import java.util.Optional;

public interface AverageRepo extends CrudRepository <Average, Integer>{

    Optional<Average> findByAgeAndSex(int age, String sex);
}
