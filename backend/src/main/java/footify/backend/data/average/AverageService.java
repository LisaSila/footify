package footify.backend.data.average;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AverageService {

    public AverageRepo averageRepo;

    @Autowired
    public AverageService(AverageRepo averageRepo) {
        this.averageRepo = averageRepo;
    }

    public Average getAverageEmission(int age, String sex) {

        if (age <= 17) {
            age = 17;
        } else if (age <= 29) {
            age = 29;
        } else if (age <= 59) {
            age = 59;
        } else {
            age = 60;
        }
        if(sex.equals("D")){
            return null;
        }

        return averageRepo.findByAgeAndSex(age, sex).get();
    }

    /**
     * Speichert initial die Durchschnittswerte in die Datenbank
     * Grundlage der Werte: https://uba.co2-rechner.de/de_DE (Umweltbundesamt)
     * Nur als Orientierungswert anzusehen
     */
    public void setAverageEmissionData() {
        /*wenn die Tabelle leer ist*/
        if (averageRepo.count() == 0) {
            averageRepo.save(new Average("M", 17, 10.66F));
            averageRepo.save(new Average("W", 17, 10.36F));
            averageRepo.save(new Average("M", 29, 10.56F));
            averageRepo.save(new Average("W", 29, 10.34F));
            averageRepo.save(new Average("M", 59, 11.61F));
            averageRepo.save(new Average("W", 59, 11.35F));
            averageRepo.save(new Average("M", 60, 11.36F));
            averageRepo.save(new Average("W", 60, 11.25F));
        }
    }
}
