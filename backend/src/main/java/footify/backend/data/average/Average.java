package footify.backend.data.average;

import footify.backend.user.User;

import javax.persistence.*;
import java.util.List;

@Entity
public class Average {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String sex;
    private int age;
    private float emission;

    public Average() {
    }

    public Average(String sex, int age, float emission) {
        this.sex = sex;
        this.age = age;
        this.emission = emission;
    }

    public float getEmission() {
        return emission;
    }
}
