package footify.backend.data.average;

public class AverageDTO {

    String sex;
    int age;

    public AverageDTO(String sex, int age) {
        this.sex = sex;
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }
}
