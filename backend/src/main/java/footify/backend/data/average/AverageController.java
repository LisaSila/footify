package footify.backend.data.average;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
public class AverageController {

    private AverageService averageService;

    @Autowired
    public AverageController(AverageService averageService) {
        this.averageService = averageService;
    }

    /**
     * Bei Backendstart wird die Methode zur Befüllung der Average Tabelle aufgerufen
     */
    @PostConstruct
    public void setAverageEmissionData() {
        averageService.setAverageEmissionData();
    }
}
