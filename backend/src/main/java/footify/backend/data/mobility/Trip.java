package footify.backend.data.mobility;

import footify.backend.data.mobility.carType.CarType;
import footify.backend.user.User;

import javax.persistence.*;

@Entity
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String transportation;

    /**
     * in Meter
     */
    private double way;

    // in Gramm
    private int emission;

    @ManyToOne
    private User user;

    @ManyToOne
    private CarType carType;

    public Trip() {
    }

    public Trip(User user, String transportation, double way, CarType carType, int emission) {
        this.transportation = transportation;
        this.way = way;
        this.user = user;
        this.carType = carType;
        this.emission = emission;
    }

    public int getId() {
        return id;
    }

    public String getTransportation() {
        return transportation;
    }

    public double getWay() {
        return way;
    }

    public int getEmission() {
        return emission;
    }

    public void setEmission(int emission) {
        this.emission = emission;
    }
}
