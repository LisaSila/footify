package footify.backend.data.mobility.carType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CarType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String vehicleType;
    private String fuelType;

    public CarType() {
    }

    public CarType(String vehicleType, String fuelType) {
        this.vehicleType = vehicleType;
        this.fuelType = fuelType;
    }

    public int getId() {
        return id;
    }
}
