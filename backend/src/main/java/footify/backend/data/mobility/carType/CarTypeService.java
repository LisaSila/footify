package footify.backend.data.mobility.carType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarTypeService {

    private CarTypeRepo carTypeRepo;

    @Autowired
    public CarTypeService(CarTypeRepo carTypeRepo) {
        this.carTypeRepo = carTypeRepo;
    }

    public CarType getCarType(CarTypeDTO carTypeDTO){
        return carTypeRepo.findByVehicleTypeAndFuelType(carTypeDTO.getVehicleType(), carTypeDTO.getFuelType()).get();
    }

    public void setVehicleData() {
        //wenn die Tabelle leer ist
        if (carTypeRepo.count() == 0) {
            carTypeRepo.save(new CarType("Kleinwagen (5 l/100 km)", "Benzin"));
            carTypeRepo.save(new CarType("Kleinwagen (5 l/100 km)", "Diesel"));
            carTypeRepo.save(new CarType("Mittelklasse (8 l/100 km)", "Benzin"));
            carTypeRepo.save(new CarType("Mittelklasse (8 l/100 km)", "Diesel"));
            carTypeRepo.save(new CarType("Oberklasse (10 l/100 km)", "Benzin"));
            carTypeRepo.save(new CarType("Oberklasse (10 l/100 km)", "Diesel"));
            carTypeRepo.save(new CarType("Kleinbus (12 l/100 km)", "Benzin"));
            carTypeRepo.save(new CarType("Kleinbus (12 l/100 km)", "Diesel"));
            carTypeRepo.save(new CarType("SUV / Sportwagen (14 l/100 km)", "Benzin"));
            carTypeRepo.save(new CarType("SUV / Sportwagen (14 l/100 km)", "Diesel"));
        }
    }
}
