package footify.backend.data.mobility;

import footify.backend.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TripRepo extends CrudRepository <Trip, Integer> {

    List<Trip> findByUser(User user);
}
