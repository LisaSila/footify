package footify.backend.data.mobility.carType;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CarTypeRepo extends CrudRepository<CarType, Integer> {

    Optional<CarType> findByVehicleTypeAndFuelType(String vehicleType, String fuelType);
}
