package footify.backend.data.mobility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TripController {

    private TripService tripService;

    @Autowired
    public TripController(TripService tripService) {
        this.tripService = tripService;
    }

    @PostMapping("/api/addTrip")
    public List<Trip> addTrip (@RequestBody TripDTO tripDTO){
        tripService.addTrip(tripDTO);
        return tripService.getTrips(tripDTO.getUsername());
    }

    @PostMapping("/api/getTrips")
    public List<Trip> getTrips (@RequestBody String username){
        List<Trip> trips;
        trips = tripService.getTrips(username);
        return trips;
    }

    @PostMapping("/api/deleteTrip")
    public boolean deleteTrip (@RequestBody int id){
        tripService.deleteTrip(id);
        return true;
    }
}
