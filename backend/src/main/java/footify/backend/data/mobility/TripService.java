package footify.backend.data.mobility;

import footify.backend.data.mobility.carType.CarType;
import footify.backend.data.mobility.carType.CarTypeService;
import footify.backend.user.User;
import footify.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TripService {

    private TripRepo tripRepo;
    private UserService userService;
    private CarTypeService carTypeService;

    @Autowired
    public TripService(TripRepo tripRepo, UserService userService, CarTypeService carTypeService) {
        this.tripRepo = tripRepo;
        this.userService = userService;
        this.carTypeService = carTypeService;
    }

    /**
     * Die Berechnungsfaktoren für Bahn, Fernbus und ÖPNV stammen von:
     * https://www.klimaneutral-handeln.de/php/kompens-berechnen.php?job=new
     * unter anderem auf Grundlage des Umweltbbundesamtes
     * <p>
     * Außerdem wird davon ausgegangen, dass Fahrradfahren (kein E-Bike) und zu Fuß gehen kein CO2(e) freisetzt,
     * weshalb ein Emissionswert von 0 als return verwendet wird.
     *
     * @param tripDTO
     */
    public void addTrip(TripDTO tripDTO) {
        int emission; // in g

        // User aus der Datenbank laden
        User user = userService.getUser(tripDTO.getUsername());

        // bei Auswahl von 'Auto' als Transportmittel:
        CarType carType = null;
        if (tripDTO.getCarTypeDTO() != null) {
            // Fahrzeugtyp aus DTO anlegen
            carType = carTypeService.getCarType(tripDTO.getCarTypeDTO());
            // Emissionen berechnen
            emission = calculateEmission(carType.getId(), tripDTO.getWay());
        } else if (tripDTO.getTransportation().equals("Bahn")) {
            emission = (int) (tripDTO.getWay() * 0.05 * 1000);
        } else if (tripDTO.getTransportation().equals("Fernbus")) {
            emission = (int) (tripDTO.getWay() * 0.03 * 1000);
        } else if (tripDTO.getTransportation().equals("Flugzeug")) {
            emission = calculateAirplaneEmission(tripDTO.getWay());
        } else if (tripDTO.getTransportation().equals("ÖPNV")) {
            emission = (int) (tripDTO.getWay() * 0.08 * 1000);
        } else {
            emission = 0;
        }

        // Trip speichern
        tripRepo.save(new Trip(user, tripDTO.getTransportation(), tripDTO.getWay(), carType, emission));
    }

    /**
     * Formel zur Berechnung: Verbrauch Liter/100 km * Strecke in km * Faktor (Benzin oder Diesel)
     * Faktoren:
     * Benzin: 2,3
     * Diesel: 2,6
     * Elektro: 0,1
     *
     * @param id
     * @param way in km
     * @return in g CO2
     */
    private int calculateEmission(int id, double way) {
        float petrol = 2.3F;
        float diesel = 2.6F;
        int emission;

        switch (id) {

            case 1:
                emission = (int) (way * (5.0 / 100.0) * petrol);
                break;
            case 2:
                emission = (int) (way * (5.0 / 100.0) * diesel);
                break;
            case 3:
                emission = (int) (way * (8.0 / 100.0) * petrol);
                break;
            case 4:
                emission = (int) (way * (8.0 / 100.0) * diesel);
                break;
            case 5:
                emission = (int) (way * (10.0 / 100.0) * petrol);
                break;
            case 6:
                emission = (int) (way * (10.0 / 100.0) * diesel);
                break;
            case 7:
                emission = (int) (way * (12.0 / 100.0) * petrol);
                break;
            case 8:
                emission = (int) (way * (12.0 / 100.0) * diesel);
                break;
            case 9:
                emission = (int) (way * (14.0 / 100.0) * petrol);
                break;
            case 10:
                emission = (int) (way * (14.0 / 100.0) * diesel);
                break;
            default:
                emission = 0;
        }
        return emission * 1000;
    }

    /**
     * Stark vereinfachte Berechnung ohne Berücksichtigung zwischen Kurz- unf Langstrecken oder Flugzeugtypen:
     * 3,9 Liter Kerosin /100 Pkm x 2,53 kg CO2 /Liter x 2,7 (RFI) = 0,27 kg CO2(äquivalent) /100 km und Person
     * Quelle: https://www.klimaneutral-handeln.de/php/kompens-berechnen.php?job=new
     * unter anderem auf Grundlage des Umweltbbundesamtes
     *
     * @param way in Personenkilometer
     * @return in g CO2e
     */
    private int calculateAirplaneEmission(double way) {
        double emission = way * 0.27;
        return (int) emission * 1000;
    }


    public List<Trip> getTrips(String username) {
        List<Trip> list = tripRepo.findByUser(userService.getUser(username));
        return list;
    }

    public void deleteTrip(int id) {
        Optional<Trip> trip = tripRepo.findById(id);
        if (trip.isPresent()) {
            tripRepo.delete(trip.get());
        }
    }
}
