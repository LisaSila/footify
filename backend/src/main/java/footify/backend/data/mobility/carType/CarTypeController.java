package footify.backend.data.mobility.carType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;

@Controller
public class CarTypeController {

    private CarTypeService carTypeService;

    @Autowired
    public CarTypeController(CarTypeService carTypeService) {
        this.carTypeService = carTypeService;
    }

    @PostConstruct
    public void setAverageEmissionData() {
        carTypeService.setVehicleData();
    }
}
