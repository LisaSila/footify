package footify.backend.data.mobility.carType;

public class CarTypeDTO {

    private String vehicleType;
    private String fuelType;

    public CarTypeDTO(String vehicleType, String fuelType) {
        this.vehicleType = vehicleType;
        this.fuelType = fuelType;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public String getFuelType() {
        return fuelType;
    }
}
