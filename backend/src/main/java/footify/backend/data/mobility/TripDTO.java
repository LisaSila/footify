package footify.backend.data.mobility;

import footify.backend.data.mobility.carType.CarTypeDTO;

public class TripDTO {

    private String username;
    private String transportation;
    private double way;
    // Name carType weil dieser im frontend gewählt wurde
    private CarTypeDTO carType;

    public TripDTO(String username, String transportation, double way, CarTypeDTO carType) {
        this.username = username;
        this.transportation = transportation;
        this.way = way;
        this.carType = carType;
    }

    public String getTransportation() {
        return transportation;
    }

    public double getWay() {
        return way;
    }

    public String getUsername() {
        return username;
    }

    public CarTypeDTO getCarTypeDTO() {
        return carType;
    }
}
