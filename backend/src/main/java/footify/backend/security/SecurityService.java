package footify.backend.security;

import footify.backend.user.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SecurityService implements UserDetailsService {


    private UserRepo userRepo;

    @Autowired
    public SecurityService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{

        Optional<footify.backend.user.User> optionalUser = userRepo.findByUsername(username);

        if(optionalUser.isPresent()) {
            return new User(optionalUser.get().getUsername(), optionalUser.get().getPassword(), List.of());
        }
        throw new UsernameNotFoundException("nö");
    }
}
