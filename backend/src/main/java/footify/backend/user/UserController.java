package footify.backend.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//REST Controller für die Kommunikation mit dem Frontend
@RestController
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/api/registration")
    public String registration(@RequestBody RegistrationDTO registrationDTO){
        return userService.register(registrationDTO);
    }

    @PostMapping("/api/setProfileSettings")
    public void setprofileSettings(@RequestBody UserSettingsDTO userSettingsDTO){
        userService.setProfileSettings(userSettingsDTO);
    }

    @PostMapping("/api/getUser")
    public User getUser(@RequestBody String username){
        return userService.getUser(username);
    }
}
