package footify.backend.user;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepo extends CrudRepository<User, Integer> {
    /**
     * Optional, weil wir nicht sicherstellen können, dass der Wert in der Datenbank vorhanden ist
     * @param username
     * @return ein Optional User (kann null sein oder ein User)
     */
    Optional<User> findByUsername(String username);
}
