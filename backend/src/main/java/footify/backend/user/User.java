package footify.backend.user;

import footify.backend.data.average.Average;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;

    private String sex;

    private int age;

    @ManyToOne
    private Average average;

    /**
     * für die Datenbank
     */
    public User(){
    }

    /**
     * zum Anlegen vom Nutzer
     */
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAverage(Average average) {
        this.average = average;
    }

    public Average getAverage() {
        return average;
    }
}
