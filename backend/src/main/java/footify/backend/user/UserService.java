package footify.backend.user;

import footify.backend.data.average.AverageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;


@Service
public class UserService {

    /**
     * Dependency Injection
     *
     * PasswordEncoder: verschlüsselt das Passwort, bevor es in die Datenbank gespeichert wird
     */
    private UserRepo userRepo;
    private AverageService averageService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(PasswordEncoder passwordEncoder, UserRepo userRepo, AverageService averageService) {
        this.passwordEncoder = passwordEncoder;
        this.userRepo = userRepo;
        this.averageService = averageService;
    }

    public String register(RegistrationDTO registrationDTO) {

        if(userRepo.findByUsername(registrationDTO.getUsername()).isPresent()){

            return "Username schon vergeben.";  // Damit kein vorhandener User gespeichert wird (Methode wird hier abgebrochen)
        }
        if(!registrationDTO.getPassword1().equals(registrationDTO.getPassword2())){
            return "Die Passwörter stimmen nicht überein.";
        }
        //try-Catch, da username und passwort die Annotation @NotEmpty haben. Wenn leer wirft es eine Exception.
        try {
            userRepo.save(new User(registrationDTO.getUsername(), this.passwordEncoder.encode(registrationDTO.getPassword1())));
        } catch (ConstraintViolationException exception) {
            return "Bitte Username und Passwort eintragen.";
        }
        return "Erfolgreich registriert.";
    }

    /**
     *
     * @param userSettingsDTO was aus den Inputfeldern kommt
     */
    public void setProfileSettings(UserSettingsDTO userSettingsDTO){

        User user = userRepo.findByUsername(userSettingsDTO.getUsername()).get();

        if(!userSettingsDTO.getPassword().equals("")){
            user.setPassword(this.passwordEncoder.encode(userSettingsDTO.getPassword()));
        }
        if(userSettingsDTO.getAge() > 0){
            user.setAge(userSettingsDTO.getAge());
        }
        if(userSettingsDTO.getSex() != null){
            user.setSex(userSettingsDTO.getSex());
        }

        if(userSettingsDTO.getAge() > 0 && userSettingsDTO.getSex() != null){
            user.setAverage(averageService.getAverageEmission(userSettingsDTO.getAge(), userSettingsDTO.getSex()));
        }
        userRepo.save(user);
    }

    /**
     *
     * @return orElse, weil das Optional in der Methode auch ein null zurückgeben könnte,
     * wenn der Username nicht gefunden wird
     */
    public User getUser(String username){
        return userRepo.findByUsername(username).orElse(null);
    }
}
