package footify.backend.user;

public class RegistrationDTO {

    private String username;
    private String password1;
    private String password2;

    public String getUsername() {
        return username;
    }

    public String getPassword1() {
        return password1;
    }
    public String getPassword2() {
        return password2;
    }
}
