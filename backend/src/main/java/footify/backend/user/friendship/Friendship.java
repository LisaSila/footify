package footify.backend.user.friendship;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Friendship {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String follower;

    private String emitter;

    public Friendship() {
    }

    public Friendship(String follower, String emitter) {
        this.follower = follower;
        this.emitter = emitter;
    }

    public String getFollower() {
        return follower;
    }

    public String getEmitter() {
        return emitter;
    }
}
