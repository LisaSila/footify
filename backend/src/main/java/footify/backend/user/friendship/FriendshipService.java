package footify.backend.user.friendship;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FriendshipService {

    private FriendshipRepo friendshipRepo;

    @Autowired
    public FriendshipService(FriendshipRepo friendshipRepo) {
        this.friendshipRepo = friendshipRepo;
    }

    public String saveFriendship(String follower, String emitter){
        if(friendshipRepo.findByFollowerAndEmitter(follower, emitter).isPresent()){
            return "Ihr seid bereits befreundet.";
        }
        Friendship friendship = new Friendship(follower, emitter);
        friendshipRepo.save(friendship);
        return "Ihr seid jetzt befreundet.";
    }

    /**
     * Friendship-Liste enthält Id, Follower, Emitter
     * beim durchiterieren durch die Liste werden nur die Emitter in eine neue ArrayList gespeichert
     * @param follower: Eingeloggter User
     * @return Liste an Emittern
     */
    public ArrayList<String> getFriends(String follower){
        ArrayList<String> emitter = new ArrayList<>();
        List<Friendship> friendships = friendshipRepo.findByFollower(follower);
        if(friendships != null){
            for (Friendship friendship : friendships) {
                emitter.add(friendship.getEmitter());
            }
        }
        return emitter;
    }

    public boolean deleteFriendship(String follower, String emitter){
        Optional<Friendship> friendship = this.friendshipRepo.findByFollowerAndEmitter(follower, emitter);
        if(friendship.isPresent()){
            this.friendshipRepo.delete(friendship.get());
        }
        return true;
    }
}
