package footify.backend.user.friendship;

import footify.backend.user.User;
import footify.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class FriendshipController {

    private FriendshipService friendshipService;
    private UserService userService;

    @Autowired
    public FriendshipController(FriendshipService friendshipService, UserService userService) {
        this.friendshipService = friendshipService;
        this.userService = userService;
    }

    @PostMapping("api/saveFriendship")
    public String saveFriendship(@RequestBody FriendshipDTO friendshipDTO){
        return friendshipService.saveFriendship(friendshipDTO.getFollower(), friendshipDTO.getEmitter());
    }

    @PostMapping("api/getFriendList")
    public List<User> getFriends(@RequestBody String follower){
        ArrayList<String> emitter = friendshipService.getFriends(follower);
        List<User> emitterUser = new ArrayList<>();
        for (String username : emitter) {
            emitterUser.add(userService.getUser(username));
        }
        return emitterUser;
    }

    @PostMapping("api/deleteFriendship")
    public boolean deleteFriendship(@RequestBody FriendshipDTO friendshipDTO){
        return friendshipService.deleteFriendship(friendshipDTO.getFollower(), friendshipDTO.getEmitter());
    }
}
