package footify.backend.user.friendship;

public class FriendshipDTO {

    private String follower;
    private String emitter;

    public String getFollower() {
        return follower;
    }

    public String getEmitter() {
        return emitter;
    }

}
