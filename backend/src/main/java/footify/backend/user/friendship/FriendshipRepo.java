package footify.backend.user.friendship;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FriendshipRepo extends CrudRepository<Friendship, Integer> {

    Optional<Friendship> findByFollowerAndEmitter(String follower, String emitter);

    List<Friendship> findByFollower(String follower);
}
