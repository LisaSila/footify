## Footify - Die CO2 einsparende Communityplattform

This project was the final project of one team of the Academy Work Academy Java Winter 2019 class.

### Teammembers (alpabethic order)


- Christoph https://gitlab.com/ChristophDoe
- Jochen https://gitlab.com/fredFeuerstein
- Kira https://gitlab.com/kiraschpunkt
- Lisa https://gitlab.com/LisaSila

### Project description

Footify is meant to be a CO2-tracking community plattform. 

The user can monitor his carbon footprint for instance the emissions produced by specific means of transportaion. 

Forthermore he can 'follow' other users to see their carbon footprint to compare CO2e emissions. 

#### Project requirements

The general objective of the final project was to use and combine all the learned skills of the bootcamp.

_demanded tech stack_:

- Java
- Spring / Spring Boot
- Maven
- JPA
- Frontend: Bulma
- Template-Engine: Thymeleaf
- Bei SPA im Frontend: Angular mit REST
- GIT

_The team also used_:

- chart.js, a free open-source JavaScript library for the data visualization
- MariaDB, an enterprise open source database solution

### Impressions

First impression of the page, when the user is logged in:

![mainpage](pictures/mainPageLogedIn.png)

It displays the main profil information of the logged in user like the name and the average CO2e emissions of a person in Germany with same sex and age as the user. 

It also provides a quick overview of all distances covered.

----

Th list below showes all distances travelled of one user. The user is able to create, edit or delete a distance travelled. 

![mobilityList](pictures/mobilityList.png)

![createWay](pictures/createWay.jpg)

----

hidden in a drop-down menu, the user can navigate to 'followed users'

![followedUser](pictures/followedUser.jpg)

----

The team also planed to include extensive statistics. Every Footify member should see the average carbon footprint of a German resident, the average of a Footify member and his own average per month.

![statistics](pictures/statistics.png)


