import { Component, OnInit } from '@angular/core';
import {MobilityService} from '../../mobility.service';
import {Router} from '@angular/router';
import {User} from '../../../user';
import {SecurityService} from '../../../security.service';

@Component({
  selector: 'app-add-trip',
  templateUrl: './add-trip.component.html',
  styleUrls: ['./add-trip.component.css']
})
export class AddTripComponent implements OnInit {

  sessionUser: User|null = null;
  public transportation = ['zu Fuß', 'Fahrrad', 'Bahn', 'Fernbus', 'Auto', 'Flugzeug', 'ÖPNV'];

  public vehicleType = ['Kleinwagen (5 l/100 km)', 'Mittelklasse (8 l/100 km)',
    'Oberklasse (10 l/100 km)', 'Kleinbus (12 l/100 km)',  'SUV / Sportwagen (14 l/100 km)'];

  public fuelType = ['Benzin', 'Diesel'];

  constructor(private mobilityService: MobilityService, private securityService: SecurityService, private router: Router) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }

  addTrip() {
    this.mobilityService.trip.username = this.sessionUser.username;
    this.mobilityService.addTrip();

    // Zurücksetzen der Werte
    this.mobilityService.trip.transportation = '';
    this.mobilityService.trip.carType = {
      vehicleType: '',
      fuelType: '',
    };
    this.mobilityService.trip.way = 0;

    if (this.mobilityService.errorMessage === null) {
      this.router.navigate(['mobilityDetails']);
    } else {
      this.mobilityService.trip.carType.vehicleType = '';
      this.mobilityService.trip.carType.fuelType = '';
    }
  }

  // Speichert Fortbewegungsmittel
  selectChangeHandlerTransportation(event: any) {
    this.mobilityService.trip.transportation = event.target.value;
  }

  // Speichert Autokategorie
  selectChangeHandlerVehicleType(event: any) {
    this.mobilityService.trip.carType.vehicleType = event.target.value;
  }

  // Speichert Kraftstofftyp
  selectChangeHandlerFuelType(event: any) {
    this.mobilityService.trip.carType.fuelType = event.target.value;
  }


}
