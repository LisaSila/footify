import { Component, OnInit } from '@angular/core';
import {User} from '../../user';
import {SecurityService} from '../../security.service';
import {Router} from '@angular/router';
import {MobilityService} from '../mobility.service';

@Component({
  selector: 'app-mobility-details',
  templateUrl: './mobility-details.component.html',
  styleUrls: ['./mobility-details.component.css']
})
export class MobilityDetailsComponent implements OnInit {

  sessionUser: User|null = null;

  constructor(private securityService: SecurityService, private mobilityService: MobilityService, private router: Router) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.mobilityService.getTrips(this.sessionUser.username);
  }

  delete(id: number) {
    this.mobilityService.deleteTrip(id);
  }
}
