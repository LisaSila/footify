export interface CarType {
  vehicleType: string;
  fuelType: string;
}
