import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {MobilityService} from './mobility.service';
import {Router} from '@angular/router';
import {Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, SingleDataSet} from 'ng2-charts';
import {ChartOptions, ChartType} from 'chart.js';
import * as Chart from 'chart.js';


@Component({
  selector: 'app-mobility',
  templateUrl: './mobility.component.html',
  styleUrls: ['./mobility.component.css']
})
export class MobilityComponent implements OnInit {

  sessionUser: User | null = null;
  private chartsloaded: boolean;
  private chartsfilled: boolean;

  groupedTrips = [
    {
      transportation: 'zu Fuß',
      way: 0,
      emission: 0
    },
    {
      transportation: 'Fahrrad',
      way: 0,
      emission: 0
    },
    {
      transportation: 'Bahn',
      way: 0,
      emission: 0
    },
    {
      transportation: 'Fernbus',
      way: 0,
      emission: 0
    },
    {
      transportation: 'Auto',
      way: 0,
      emission: 0
    },
    {
      transportation: 'Flugzeug',
      way: 0,
      emission: 0
    },
    {
      transportation: 'ÖPNV',
      way: 0,
      emission: 0
    },
  ];

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      display: false
    },
  };
  public pieChartLabels: Label[] = [['zu Fuß'], ['Fahrrad'], ['Bahn'], ['Fernbus'], ['Auto'], ['Flugzeug'], ['ÖPNV']];
  public pieChartDataWay: SingleDataSet = [0, 0, 0, 0, 0, 0, 0];
  public pieChartDataEmission: SingleDataSet = [0, 0, 0, 0, 0, 0, 0];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  constructor(private securityService: SecurityService,
              private mobilityService: MobilityService,
              private router: Router) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.getTrips();
  }

  /**
   * Summe pro Transportmittel
   */
  public groupTrips() {
    if (this.mobilityService.trips !== null) {
      for (const trip of this.mobilityService.trips) {
        for (const transportation in this.groupedTrips) {
          if (trip.transportation === this.groupedTrips[transportation].transportation) {
            this.groupedTrips[transportation].emission += trip.emission;
            this.groupedTrips[transportation].way += trip.way;
          }
        }
      }
      this.groupTripsPercent();
    }
  }

  /**
   * 1. For Schleife:
   * Summe über alle Transportmittel hinweg
   *
   * 2. For Schleife:
   * Umrechnung in Prozent (Wert/ Gesamtwert * 100)
   *
   * If Abfrage:
   * damit die "Warnung", dass keine Daten vorhanden sind nicht mehr erscheint
   *
   * Chartloaded = true:
   * boolean um zu kennzeichnen, wann die Berechnung fertig ist (html)
   */
  private groupTripsPercent() {
    let sumEmission: number;
    let sumWay: number;
    sumEmission = 0;
    sumWay = 0;
    for (const transportation in this.groupedTrips) {
      sumEmission += this.groupedTrips[transportation].emission;
      sumWay += this.groupedTrips[transportation].way;
    }
    let i: number;
    i = 0;
    for (const transportation in this.groupedTrips) {
      this.pieChartDataEmission[i] = Math.round(this.groupedTrips[transportation].emission / sumEmission * 100);
      this.pieChartDataWay[i] = Math.round(this.groupedTrips[transportation].way / sumWay * 100);
      i++;
    }
    if (sumEmission !== 0 && sumWay !== 0) {
      this.chartsfilled = true;
    }
    this.chartsloaded = true;
  }

  private getTrips() {
    if (this.sessionUser !== null) {
      this.mobilityService.getTrips(this.sessionUser.username);
      setTimeout(() => this.groupTrips(), 500);
    }
  }
}
