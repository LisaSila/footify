import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Trip} from './trip';
import {ProfileService} from '../profile/profile.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MobilityService {

  public trip: Trip = {
    id: 0,
    username: '',
    transportation: '',
    way: 0,
    emission: 0,
    carType: {
      vehicleType: '',
      fuelType: ''
    }
  };

  public trips: Trip[] = null;

  errorMessage: string = null;

  constructor(private http: HttpClient, private router: Router) {
  }

  public addTrip() {
    this.errorMessage = null;

    // Abfrage, ob Fahrzeugtyp oder Kraftstoffart fehlt
    if (this.trip.transportation === 'Auto' && (this.trip.carType.vehicleType === '' || this.trip.carType.fuelType === '')) {
      this.errorMessage = 'Das hat leider nicht geklappt. Bitte gib ein Fahrzeugtyp und eine Kraftstoffart an.';
      return;
    } else if (this.trip.carType.vehicleType === '' && this.trip.carType.fuelType === '') {
      // Wenn nicht das Fortbewegungsmittel 'Auto' gewählt wurde
      this.trip.carType = null;
    }

    if (this.trip.transportation !== '' && this.trip.way !== 0) {
      this.http.post<Trip[]>('/api/addTrip', this.trip).subscribe(
        list => this.trips = list
      );

    } else {
      this.errorMessage = 'Das hat leider nicht geklappt. Bitte gib eine Strecke und eine Fortbewegungsart an.';
    }
  }

  getTrips(username: string) {
    this.http.post<Trip[]>('/api/getTrips', username).subscribe(
      list => this.trips = list
    );
  }

  deleteTrip(id: number) {
    this.http.post<boolean>('/api/deleteTrip', id).subscribe( bool => this.reloadTripList(bool));
  }

  reloadTripList(bool: boolean) {
    this.getTrips(this.trip.username);
  }

}
