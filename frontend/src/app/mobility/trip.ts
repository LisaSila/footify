import {CarType} from './carType';

export interface Trip {
  id: number;
  username: string;
  transportation: string;
  way: number;
  carType: CarType;
  emission: number;
}

