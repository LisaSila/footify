import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../user';
import {SecurityService} from '../security.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  registerMessage: string = null;

  sessionUser: User|null = null;
  registration = {
    username: '',
    password1: '',
    password2: '',
  };

  constructor(private http: HttpClient, private securityService: SecurityService) { }

  /**
   * u ist der Returnwert von getSessionUser
   */
  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.registerMessage = null;
  }

  /**
   * der String der Postmethode ist ihr Rückgabewert
   */
  register() {
    this.http.post<string>('/api/registration', this.registration, {responseType: 'text' as 'json'}).subscribe(
      message => this.registerMessage = message
    );
    this.registration.username = '';
    this.registration.password1 = '';
    this.registration.password2 = '';
  }
}
