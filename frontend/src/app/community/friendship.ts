export interface Friendship {
  follower: string;
  emitter: string;
}
