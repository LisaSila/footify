import {Injectable} from '@angular/core';
import {CompleteUser} from '../profile/completeUser';
import {ProfileService} from '../profile/profile.service';
import {HttpClient} from '@angular/common/http';
import {Friendship} from './friendship';
import {MobilityService} from '../mobility/mobility.service';
import {Trip} from '../mobility/trip';

@Injectable({
  providedIn: 'root'
})
export class CommunityService {

  public friend: CompleteUser = null;
  public friendsTripList: Trip[] = null;
  public friendList: CompleteUser[] = null;
  private friendship: Friendship;
  public message: string;
  public errorMessage: string;

  constructor(private profileService: ProfileService,
              private mobilityService: MobilityService,
              private http: HttpClient) {
  }

  public getFriend(username: string) {
    this.http.post<CompleteUser>('/api/getUser', username).subscribe(
      user => this.loadFriendsProfile(user));
  }

  loadFriendsProfile(friend: CompleteUser) {
    this.errorMessage = null;
    this.friend = friend;
    if (this.friend === null) {
      this.errorMessage = 'Den Nutzer gibt es nicht.';
    }
    this.getFriendsTrips(friend.username);
  }

  public getFriendList(follower: string) {
    this.http.post<CompleteUser[]>('/api/getFriendList', follower).subscribe(
      list => this.friendList = list,
    );
  }

  saveFriend(username: string) {
    this.friendship = {
      follower: username,
      emitter: this.friend.username,
    };
    this.http.post<string>('/api/saveFriendship', this.friendship, {responseType: 'text' as 'json'}).subscribe(
      message => this.getFriendsAfterSave(message, username),
    );
  }

  getFriendsAfterSave(message: string, username: string) {
    this.message = message;
    this.getFriendList(username);
  }

  getFriendsTrips(username: string) {
    this.http.post<Trip[]>('/api/getTrips', username).subscribe(
      list => this.friendsTripList = list
    );
  }

  deleteFriend(follower1: string, emitter1: string) {
    const friendship: Friendship = {
      follower: follower1,
      emitter: emitter1,
    };
    this.http.post<boolean>('/api/deleteFriendship', friendship).subscribe( bool => this.reloadFriendList(follower1));
  }

  reloadFriendList(follower: string) {
    this.getFriendList(follower);
  }
}
