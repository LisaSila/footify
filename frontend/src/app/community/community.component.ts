import { Component, OnInit } from '@angular/core';
import {CommunityService} from './community.service';
import {Router} from '@angular/router';
import {User} from '../user';
import {SecurityService} from '../security.service';

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.css']
})
export class CommunityComponent implements OnInit {

  sessionUser: User | null = null;

  constructor(private communityService: CommunityService, private  router: Router, private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.getFriendList(u)
    );
  }

  private getFriendList(user: User) {
    this.sessionUser = user;
    this.communityService.getFriendList(user.username);
  }

  getFriendsProfile(friendsName: string) {
    this.communityService.getFriend(friendsName);
    setTimeout(() => this.router.navigate(['/community/friendsProfile']), 500);
  }

  deleteFriend(follower: string, emitter: string) {
    this.communityService.deleteFriend(follower, emitter);
  }
}
