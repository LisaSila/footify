import { Component, OnInit } from '@angular/core';
import {SecurityService} from '../../security.service';
import {User} from '../../user';
import {CommunityService} from '../community.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search-friend',
  templateUrl: './search-friend.component.html',
  styleUrls: ['./search-friend.component.css']
})
export class SearchFriendComponent implements OnInit {

  sessionUser: User | null = null;
  friendName: string;

  constructor(private securityService: SecurityService, private communityService: CommunityService, private router: Router) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.communityService.friend = null;
  }

  getFriend() {
    this.communityService.getFriend(this.friendName);
  }

  out() {
    this.communityService.message = null;
    this.communityService.errorMessage = null;
    this.router.navigate(['/community']);
  }

  saveFriend() {
    this.communityService.saveFriend(this.sessionUser.username);
  }
}
