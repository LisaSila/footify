import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ChartsModule} from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SessionUserComponent } from './session-user/session-user.component';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import { RegistrationComponent } from './registration/registration.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import {MobilityDetailsComponent} from './mobility/mobility-details/mobility-details.component';
import {ProfileComponent} from './profile/profile.component';
import { ChangeProfileDetailsComponent } from './profile/change-profile-details/change-profile-details.component';
import { AddTripComponent} from './mobility/mobility-details/add-trip/add-trip.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { MobilityComponent } from './mobility/mobility.component';
import { CommunityComponent } from './community/community.component';
import { SearchFriendComponent } from './community/search-friend/search-friend.component';
import { FriendsProfileComponent } from './community/friends-profile/friends-profile.component';
import { StatisticsComponent } from './statistics/statistics.component';

@NgModule({
  declarations: [
    AppComponent,
    SessionUserComponent,
    LoginComponent,
    RegistrationComponent,
    LandingPageComponent,
    MobilityComponent,
    ProfileComponent,
    ChangeProfileDetailsComponent,
    AddTripComponent,
    ContactUsComponent,
    MobilityDetailsComponent,
    CommunityComponent,
    SearchFriendComponent,
    FriendsProfileComponent,
    StatisticsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
