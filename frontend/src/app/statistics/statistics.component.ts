import { Component, OnInit } from '@angular/core';
import {SecurityService} from '../security.service';
import {User} from '../user';
import {Router} from '@angular/router';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Label} from 'ng2-charts';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  sessionUser: User | null = null;

  barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [{
        type: 'category',
      }],
      yAxes: [{
        stacked: true,
      }]
    }
  };
  barChartLabels: Label[] = ['Jan', 'Feb', 'Mäz', 'Apr', 'Jun', 'Jul', 'Aug', 'Sep'];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  /**
   * Dummywerte
   */
  barChartDataTotalEmissionGer: ChartDataSets[] = [{
    data: [
      {x: 'Jan', y: 1010},
      {x: 'Feb', y: 1020},
      {x: 'Mär', y: 1030},
      {x: 'Apr', y: 1040},
      {x: 'Jun', y: 1030},
      {x: 'Jul', y: 1030},
      {x: 'Aug', y: 1020},
      {x: 'Sep', y: 1020},
    ],
    label: 'CO2e in kg',
    backgroundColor: '#93d9d9', /*wie in Pie Auto*/
    hoverBackgroundColor: '#c1d6e1', /*wie in Pie Airplane*/
    borderColor: '#93d9d9',
  }];

  /**
   * Dummywerte
   */
  barChartDataTotalEmissionFooify: ChartDataSets[] = [{
    data: [
      {x: 'Jan', y: 1010},
      {x: 'Feb', y: 1000},
      {x: 'Mär', y: 980},
      {x: 'Apr', y: 970},
      {x: 'Jun', y: 970},
      {x: 'Jul', y: 975},
      {x: 'Aug', y: 960},
      {x: 'Sep', y: 965},
    ],
    label: 'CO2e in kg',
    backgroundColor: '#93d9d9', /*wie in Pie Auto*/
    hoverBackgroundColor: '#c1d6e1', /*wie in Pie Airplane*/
    borderColor: '#93d9d9',
  }];

  /**
   * Dummywerte
   */
  barChartDataTotalEmissionUser: ChartDataSets[] = [{
    data: [
      {x: 'Jan', y: 1040},
      {x: 'Feb', y: 1033},
      {x: 'Mär', y: 1033},
      {x: 'Apr', y: 1020},
      {x: 'Jun', y: 1010},
      {x: 'Jul', y: 950},
      {x: 'Aug', y: 945},
      {x: 'Sep', y: 930},
    ],
    label: 'CO2e in kg',
    backgroundColor: '#93d9d9', /*wie in Pie Auto*/
    hoverBackgroundColor: '#c1d6e1',
    borderColor: '#93d9d9',
  }];

  constructor(private securityService: SecurityService, private router: Router) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }


}
