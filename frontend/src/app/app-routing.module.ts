import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RegistrationComponent} from './registration/registration.component';
import {LoginComponent} from './login/login.component';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {ProfileComponent} from './profile/profile.component';
import {MobilityDetailsComponent} from './mobility/mobility-details/mobility-details.component';
import {ChangeProfileDetailsComponent} from './profile/change-profile-details/change-profile-details.component';
import {AddTripComponent} from './mobility/mobility-details/add-trip/add-trip.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {MobilityComponent} from './mobility/mobility.component';
import {CommunityComponent} from './community/community.component';
import {SearchFriendComponent} from './community/search-friend/search-friend.component';
import {FriendsProfileComponent} from './community/friends-profile/friends-profile.component';
import {StatisticsComponent} from './statistics/statistics.component';


const routes: Routes = [
  {path: '', component: LandingPageComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'profile', component: ProfileComponent,
    children: [
      {path: 'changeProfileDetails', component: ChangeProfileDetailsComponent},
    ]
  },
  {path: 'mobility', component: MobilityComponent},
  {path: 'mobilityDetails', component: MobilityDetailsComponent,
      children: [
        {path: 'addTrip', component: AddTripComponent},
      ]},

  {path: 'community', component: CommunityComponent,
  children: [
  {path: 'searchFriend', component: SearchFriendComponent},
  {path: 'friendsProfile', component: FriendsProfileComponent},
]},

  {path: 'statistics', component: StatisticsComponent},
  {path: 'contact', component: ContactUsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
