import {Component, OnInit} from '@angular/core';
import {User} from './user';
import {SecurityService} from './security.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'FOOTify';
  sessionUser: User | null = null;

  constructor(private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }

  logout() {
    this.securityService.logout();
  }

}
