import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from './user';
import {ProfileService} from './profile/profile.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  private sessionUser = new BehaviorSubject<User|null>(null);
  public message: string = null;
  public loginData = {
    username: '',
    password: ''
  };

  constructor(private httpClient: HttpClient, private profileService: ProfileService, private router: Router) {
    this.httpClient.get<User>('/api/sessionUser').subscribe(
      u => this.sessionUser.next(u)
    );
  }

  /**
   * nach dem : Rückgabetyp
   */
  public getSessionUser(): Observable<User|null> {
    return this.sessionUser;
  }

  public login() {
    this.httpClient.get<User>('/api/sessionUser', {
      headers: {
        authorization : 'Basic ' + btoa(this.loginData.username + ':' + this.loginData.password)
      }
    }).subscribe(
      u => this.isSessionUserActive(u),
      () => this.sessionUser.next(null),
    );
  }

  // man bleibt nicht im Login stecken
  isSessionUserActive(sesssionUser: User) {
    this.sessionUser.next(sesssionUser);
    this.clear();
        if (this.sessionUser !== null) {
          this.router.navigate(['']);
        } else {
          this.message = 'Username und Passwort sind nicht kompatibel.';
        }
  }

  clear() {
    this.loginData.username = '';
    this.loginData.password = '';
  }

  public logout() {
    this.message = null;
    this.httpClient.post('/api/logout', null).subscribe(
      () => this.sessionUser.next(null),
    );
    this.profileService.completeUser = null;
    this.router.navigate(['']);
  }
}
