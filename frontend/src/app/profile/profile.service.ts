import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CompleteUser} from './completeUser';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  public completeUser: CompleteUser = {
    username: null,
    password: null,
    sex: null,
    age: 0,
    average: null,
  };

  constructor(private http: HttpClient) {
  }

  public getUser(username: string) {
    this.http.post<CompleteUser>('/api/getUser', username).subscribe(
      user => this.completeUser = user);
  }

  public setProfileSettings() {
    this.http.post('/api/setProfileSettings', this.completeUser).toPromise().then(r => this.getUser(this.completeUser.username));
  }
}
