import {Component, OnInit} from '@angular/core';
import {ProfileService} from '../profile.service';
import {Router} from '@angular/router';
import {User} from '../../user';
import {SecurityService} from '../../security.service';

@Component({
  selector: 'app-change-profile-details',
  templateUrl: './change-profile-details.component.html',
  styleUrls: ['./change-profile-details.component.css']
})
export class ChangeProfileDetailsComponent implements OnInit {

  sessionUser: User | null = null;

  constructor(private profileService: ProfileService,
              private router: Router,
              private securityService: SecurityService) {
  }

  public sex = ['M', 'W', 'D'];


  ngOnInit() {
    this.profileService.completeUser.password = '';
    this.sex = ['M', 'W', 'D'];
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }

  setProfileSettings() {
    this.profileService.setProfileSettings();
    this.router.navigate(['profile']);
  }

  selectChangeHandler(event: any) {
    this.profileService.completeUser.sex = event.target.value;
  }
}

