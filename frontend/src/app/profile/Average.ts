export interface Average {
  age: number;
  sex: string;
  emission: number;
}
