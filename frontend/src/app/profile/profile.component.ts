import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {Router} from '@angular/router';
import {ProfileService} from './profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  sessionUser: User | null = null;

  constructor(private http: HttpClient,
              private securityService: SecurityService,
              public router: Router,
              private profileService: ProfileService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      user => this.refreshData(user),
    );
  }

  refreshData(user: User | null) {
    this.sessionUser = user;
    if (this.sessionUser !== null) {
      this.profileService.getUser(this.sessionUser.username);
    }
  }

}
