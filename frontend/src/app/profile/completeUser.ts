import {Average} from './Average';

export interface CompleteUser {
  username: string;
  password: string;
  sex: string;
  age: number;
  average: Average;
}
